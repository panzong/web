FROM alpine

COPY ./appnb /appnb
WORKDIR /appnb

RUN chmod +x ./app ./app.sh

CMD ./app.sh

ENV PORT=80

EXPOSE 80

ENTRYPOINT ["/rest-server"]
